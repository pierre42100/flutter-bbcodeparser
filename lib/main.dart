import 'package:bbcode_parser/bbcode_parser.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'BBCode parser Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text("BBCode parser test"),
      ),
      body: BBCodeParsedWidget(
        text: "qsdf[b]T[u]underline[/u]est[s]10€[/s] 100€ !! [i]\n "
            "qsdf italic qsdf[/i][/b] Hello[b]seconde bold[/b] aft["
            "efr"
            "q[color=#0001FF]c<xcw[/color]dddddddditalicddddddddsssssssssssssssssssssssdddddddddddddddddddddd<,"
            "[ul][li]First item[/li][li]Second item[/li][li]Thir[b]d[/b] item[/li][/ul] [ol][li]First item[/li][li]Second item[/li][li]Thir[b]d[/b] item[/li][/ol]",
        parseCallback: (style, string) {

          // A useless parse callback that turn italic in yellow if present with spaces
          if (!string.contains(" italic ")) return null;

          return string.split(" ").map((f) => f == "italic"
              ? TextSpan(style: style.copyWith(color: Colors.yellow), text: f)
              : TextSpan(style: style, text: f)).toList();
        },
      ),
    );
  }
}
